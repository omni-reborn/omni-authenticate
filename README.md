# Omni Service Boilerplate

![Omni Reborn Service Boilerplate](docs/images/service-banner.png)

‏‏‎Version: 0.0.1 (Pre-Alpha)\
Author: Kamil Solecki 

This is a boilerplate for development of services for Omni Reborn (link needed) -the most universal Discord bot available.

Remember to check all build-related files and input values relative to your service.

![Contents](docs/images/service-contents.png)

---

Detailed explanations:
 - [Configuration files (and how to implement new ones)](docs/config.md)
 - [Container (and how to register things in it)](docs/container.md)
 - [Controllers (HTTP Controllers, Domain Controllers and Transactions)](docs/controller.md)

Stack:
- [x] Typescript 3.7.5
- [x] Typeorm 0.2.22
- [x] Inversify 5.0.1
- [x] Gulp 4.0.2
- [x] Mocha 7.0.1
- [x] and more...

![How to develop](docs/images/service-how-to-develop.png)

---

To begin, install required dependencies:
```
yarn
```

From then, you have access to multiple pre-made scripts:
```ts
yarn run compose-build  // runs docker-compose build
yarn run compose-run    // runs docker-compose up
yarn run build          // builds the application for production
yarn run test           // runs tests using mocha
yarn run coverage       // runs test coverage using istanbul
yarn run typeorm        // typeorm proxy script to work with ts directly
```

This project also features a pre-made kubernetes deployment available in `deployment.yaml`, as well as a full docker configuration, available in `Dockerfile`.

For development, we recommend using `docker-compose`, as it uses our docker image which is close to how it will end up looking in production.

![Config](docs/images/service-config.png)

---
Project configuration files are located in `/config`, and are written in YAML.
They usually reference environment variables using `!!env` declaration, which are, during development, made available to the application through docker-compose and sourced from the `.env` file.

In production, environment variables should be published on the cluster in the same namespace as the application deployment, in form of [secrets](https://kubernetes.io/docs/concepts/configuration/secret/).

### **Important!**

Configuration files are referenced in the [config module](/src/container/configModule.ts) by filename. Moreover, they require runtime type definitions (using [io-ts](https://github.com/gcanti/io-ts)). When creating new configuration files, remember to write both their runtime type definitions and register them in the container for injection.

![License](docs/images/service-contributing.png)

---

This is an open-source project, and all contributions and issues are more than welcome.
However, I'd appreciate if above-mentioned followed the following guideline:

- Please submit your issues and PRs to the [Gitlab mirror of this repository](https://gitlab.com/omni-reborn/omni-service-boilerplate).
- All PRs undergo automatic testing, and only PRs which pass tests will be merged.
- If a PR introduces new features, a minimum of 80% test coverage of those features is required.
- When raising an issue, please provide an appropriate reproduction process of the bug (if applicable).

![License](docs/images/service-license.png)

---
This project is distributed under the [MIT License](LICENSE).