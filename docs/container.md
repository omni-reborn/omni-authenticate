# Container

![Omni Reborn Service Boilerplate](images/service-banner-wide.png)

Services are based on [InversifyJS](https://github.com/inversify/InversifyJS).
Everything is registered in the container and appropriately injected into different classes and subservices. To facilitate that, we use identifiers located in `src/constants`.

Container is resolved once in the project root ([index.ts](src/index.js)) and it is the only place where things should be resolved directly from it using `container.get()`. Do otherwise, and you end up with a service locator.

![Omni Reborn Service Boilerplate](images/service-registering-in-container.png)

---

To register a new application service (or anything) in the container, please use modules located in `src/container` - or if not fitting, create a new one.
Container modules can be synchronous or asynchronous.

To register a new thing in the container, there is a few things you need to do:
1. Write an implementation and mark it as `injectable()`:

```ts
export interface IExampleFactory {
  createExample(): void;
}

@injectable()
export class ExampleFactory implements IExampleFactory {
  public createExample() {
    // create thing
  }
}
```

2. Create an indentifier (by convention, it should match the interface name):
```ts
export const FACTORY_IDENTIFIER = {
  IExampleFactory: Symbol.for('IExampleFactory'),
};
```

3. Register it in the container:
```ts
export const factoryModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IExampleFactory>(SERVICE_IDENTIFIER.IExampleFactory)
      .to(ExampleFactory)
      .inRequestScope();
);
```

If you want to learn more about working with InversifyJS, [go here!](https://github.com/inversify/InversifyJS)