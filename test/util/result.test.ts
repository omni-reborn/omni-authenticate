import * as chai from 'chai';
import 'mocha';

const expect = chai.expect;
import { Result } from 'src/util';
import { InvalidOperationError } from 'src/error/core';
import { expectToThrow } from 'test/expectToThrow';

describe('Result', () => {
  it('Should be able to getValue() if successful', () => {
    const result = new Result<string>(true, null, 'success');
    expect(result.getValue()).to.be.equal('success');
  });

  it('Should be able to get errorValue() if not successful', () => {
    const result = new Result<string>(false, 'failure', 'success');
    expect(result.errorValue()).to.be.equal('failure');
  })

  it('combine() of successful results should return a successful result', () => {
    const result1 = new Result<string>(true, null, 'success1');
    const result2 = new Result<string>(true, null, 'success2');
    const combined = Result.combine([result1, result2]);

    expect(combined.isSuccess).to.be.true;
    expect(combined.isFailure).to.be.false;
  });

  it('combine() of a successful result and a failing one should return a failing result', () => {
    const result1 = new Result<string>(true, null, 'success1');
    const result2 = new Result<string>(false, 'failure2', 'success2');
    const combined = Result.combine([result1, result2]);

    expect(combined.isSuccess).to.be.false;
    expect(combined.isFailure).to.be.true;
  });

  it('ok() should return a new result that is successful', () => {
    const result = Result.ok();

    expect(result.isSuccess).to.be.true;
    expect(result.isFailure).to.be.false;
  });

  it('fail() should return a new result that is failing and have an error', () => {
    const result = Result.fail('failure');

    expect(result.isSuccess).to.be.false;
    expect(result.isFailure).to.be.true;
  });

  it('Should throw an InvalidOperationError when attempting to create an error that is successful and pass an error', () => {
    expectToThrow(() => new Result(true, 'error'), InvalidOperationError);
  });

  it('Should throw an InvalidOperationError when attempting to create an error that is failing and not pass an error', () => {
    expectToThrow(() => new Result(false, null), InvalidOperationError);
  });

  it('Should throw an InvalidOperationError when attempting to getValue() of a failing result', () => {
    expectToThrow(() => new Result(false, 'failure').getValue(), InvalidOperationError);
  });
});
