import * as chai from 'chai';
import 'mocha';

const expect = chai.expect;
import { Guard } from 'src/util';

describe('Guard', () => {
  it('againstNullOrUndefined() should return success when argument is not nullish', () => {
    const result = Guard.againstNullOrUndefined('test', 'test');
    expect(result).to.deep.equal({ succeeded: true });
  });

  it('againstNullOrUndefined() should return failure when argument is nullish', () => {
    const result = Guard.againstNullOrUndefined(null, 'test');
    expect(result).to.have.property('succeeded', false);
  });

  it('againstNullOrUndefinedBulk() should return success when all arguments are not nullish', () => {
    const result = Guard.againstNullOrUndefinedBulk([
      { argument: 'test1', argumentName: 'test1' },
      { argument: 'test2', argumentName: 'test2' },
    ]);

    expect(result).to.deep.equal({ succeeded: true });
  });

  it('againstNullOrUndefinedBulk() should return failure when at least one argument is nullish', () => {
    const result = Guard.againstNullOrUndefinedBulk([
      { argument: 'test1', argumentName: 'test1' },
      { argument: null, argumentName: 'test2' },
    ]);

    expect(result).to.have.property('succeeded', false);
  });

  it('isOneOf() should return success if provided value satisfies the constraint', () => {
    const result = Guard.isOneOf(2, [1, 2, 3], '2');
    expect(result).to.deep.equal({ succeeded: true });
  });

  it('isOneOf() should return failure if provided value does not satisfy the constraint', () => {
    const result = Guard.isOneOf(2, [1, 3], '2');
    expect(result).to.have.property('succeeded', false);
  });

  it('inRange() should return success if provided number is in range', () => {
    const result = Guard.inRange(2, 1, 3, '2');
    expect(result).to.deep.equal({ succeeded: true });
  });

  it('inRange() should return failure if provided number is not in range', () => {
    const result = Guard.inRange(3, 1, 2, '3');
    expect(result).to.have.property('succeeded', false);
  });

  it('allInRange() should return success if all provided numbers are in range', () => {
    const result = Guard.allInRange([2, 3, 4], 1, 5, '2, 3, 4');
    expect(result).to.deep.equal({ succeeded: true });
  });

  it('allInRange() should return failure if any of the provided numbers is out of range', () => {
    const result = Guard.allInRange([2, 3, 7], 1, 5, '2, 3, 7');
    expect(result).to.have.property('succeeded', false);
  });

  it('combine() should return success if all guard results are successful', () => {
    const result = Guard.combine([
      Guard.againstNullOrUndefined(true, 'some boolean'),
      Guard.againstNullOrUndefined(5, 'some number'),
    ]);

    expect(result).to.deep.equal({ succeeded: true });
  });

  it('combine() should return the first failing guard result if one exists', () => {
    const successful = Guard.againstNullOrUndefined(true, 'some boolean');
    const failing = Guard.againstNullOrUndefined(null, 'some null');

    const result = Guard.combine([successful, failing]);
    expect(result).to.deep.equal(failing);
  });
});
