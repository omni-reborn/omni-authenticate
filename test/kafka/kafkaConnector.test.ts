import * as chai from 'chai';
import * as sinon from 'sinon';
import 'mocha';

const expect = chai.expect;

import { KafkaConnector, IKafkaConnector } from 'src/kafka';
import { mockConfig } from './mockConfig';
import { Kafka } from 'kafkajs';

describe('Kafka connector', () => {
  const sandbox = sinon.createSandbox();

  let connector: IKafkaConnector;

  beforeEach(() => {
    connector = new KafkaConnector(mockConfig);
  });

  afterEach(() => {
    sandbox.restore();
  });

  it('getClient() should create a client when none exists', () => {
    const client = connector.getClient();
    expect(client).to.be.instanceOf(Kafka);
  });

  it('getClient() should return the same client if called more than once', () => {
    const client1 = connector.getClient();
    const client2 = connector.getClient();

    expect(client1).to.be.equal(client2);
  });
});
