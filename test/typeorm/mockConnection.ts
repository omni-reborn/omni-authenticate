import * as sinon from 'sinon';
import { EntityManager } from 'typeorm';

export interface IMockConnection {
  transaction<T>(runInTransaction: (entityManager: EntityManager) => Promise<T>): Promise<T>;
}

export const mockConnection: IMockConnection = {
  transaction: sinon.stub().yields(sinon.createStubInstance(EntityManager)),
};
