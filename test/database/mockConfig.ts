import { IDatabaseConnectorConfig } from 'src/config/database';

export const mockConfig: IDatabaseConnectorConfig = {
  type: 'postgres',
  host: 'localhost',
  port: '5432',
  username: 'test',
  password: 'test',
  database: 'test',
};
