import * as chai from 'chai';
import 'mocha';

const expect = chai.expect;

import { envType } from 'src/config/core';
import { NotFoundError } from 'src/error/core';
import { expectToThrow } from 'test/expectToThrow';

describe('Custom config !env type', () => {
  beforeEach(() => {
    process.env.TOKEN = 'test';
  });

  afterEach(() => {
    delete process.env.TOKEN;
  });

  it('Should throw NotFoundError on missing variables', () => {
    expectToThrow(() => envType.resolve('DOES_NOT_EXIST'), NotFoundError);
  });

  it('Should resolve existing variables', () => {
    expect(envType.resolve('TOKEN')).to.equal(true);
  });

  it('Should construct a value from env variable', () => {
    expect(envType.construct('TOKEN')).to.equal('test');
  });
});
