import { ILoggerConfig } from 'src/config/logger';

export const mockConfig: ILoggerConfig = {
  defaults: {
    level: true,
    timestamp: true,
    module: true,
  },
}
