import * as chai from 'chai';
const expect = chai.expect;

/* tslint:disable: callable-types */
export async function expectToThrowAsync(func: (...args: Array<any>) => Promise<any>, errorConstructor: { new(...args: Array<any>): Error }) {
  let error = null;

  try { await func(); } catch (err) { error = err; }
  expect(error).to.be.instanceOf(errorConstructor);
}

export function expectToThrow(func: (...args: Array<any>) => any, errorConstructor: { new(...args: Array<any>): Error }) {
  let error = null;

  try { func(); } catch (err) { error = err; };
  expect(error).to.be.instanceOf(errorConstructor);
}
