import { Request, Response } from 'express';
import { controller as httpController, interfaces, httpPost, request, response, httpGet } from 'inversify-express-utils';
import { inject } from 'inversify';
import { CONTROLLER_IDENTIFIER, SERVICE_IDENTIFIER, APP_IDENTIFIER } from 'src/constants';
import { IUserController } from 'src/controller/domain';
import { ILogger } from 'src/logger';
import { exists, existsBulk } from 'src/util';
import { InvalidInviteError, UsernameTakenError, UserNotFoundError, InvalidUserPassword, InvalidAccountTypeError } from 'src/error/controller';
import { IDiscordApiService } from 'src/service/discordApi';

@httpController('/user')
export class HttpUserController implements interfaces.Controller {
  private readonly logger: ILogger;
  private readonly userController: IUserController;
  private readonly discordApiService: IDiscordApiService;

  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(CONTROLLER_IDENTIFIER.IUserController) userController: IUserController,
    @inject(SERVICE_IDENTIFIER.IDiscordApiService) discordApiService: IDiscordApiService,
  ) {
    this.logger = logger;
    this.userController = userController;
    this.discordApiService = discordApiService;
  }

  @httpPost('/login/client')
  private async loginClientUser(@request() req: Request, @response() res: Response) {
    const code = req.body.code;
    if (!exists(code)) {
      res.status(400).json({ message: 'Missing discord grant code.' });
    }

    try {
      const jwt = await this.userController.loginClientUser(code);
      res.cookie('OmniPanelUser', jwt);
      res.status(200).json({ message: 'Logged in successfully.' });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Internal server error.' });
    }
  }
}
