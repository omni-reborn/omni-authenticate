import { injectable, inject } from 'inversify';
import { REPOSITORY_IDENTIFIER, SERVICE_IDENTIFIER } from 'src/constants';
import { Permission, Role } from 'src/entity';
import { ILogger } from 'src/logger';
import { IRoleRepository, IPermissionRepository } from 'src/repository';
import { differenceWith } from 'lodash';
import { exists } from 'src/util';

export interface SaveRoleProps {
  name: string;
  level: number;
  permissionStrings: Array<string>;
}

export interface IRoleController {
  saveRole(props: SaveRoleProps): Promise<void>;
}

@injectable()
export class RoleController implements IRoleController {
  private readonly logger: ILogger;
  private readonly roleRepository: IRoleRepository;
  private readonly permissionRepository: IPermissionRepository;

  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(REPOSITORY_IDENTIFIER.IRoleRepository) roleRepository: IRoleRepository,
    @inject(REPOSITORY_IDENTIFIER.IPermissionRepository) permissionRepository: IPermissionRepository,
  ) {
    this.permissionRepository = permissionRepository;
    this.roleRepository = roleRepository;
  }

  private async fetchPermissions(permissionStrings: Array<string>) {
    const fetched = await this.permissionRepository.getByPermissionStringBulk(permissionStrings);
    const toCreate = differenceWith(permissionStrings, fetched, (a, b) => a === b.permissionString);

    const created = toCreate.map((permissionString) => Permission.create({ permissionString }));
    return [...created, ...fetched];
  }

  public async saveRole(props: SaveRoleProps) {
    const permissions = await this.fetchPermissions(props.permissionStrings);
    const fetchedRole = await this.roleRepository.getByName(props.name);

    for (const permission of permissions) {
      await this.permissionRepository.save(permission);
    }

    if (exists(fetchedRole)) {
      fetchedRole.permissions = permissions;
      await this.roleRepository.save(fetchedRole);
    } else {
      const role = Role.create({
        name: props.name,
        level: props.level,
        permissions,
      });

      await this.roleRepository.save(role);
    }
  }
}
