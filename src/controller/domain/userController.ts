import { injectable, inject } from 'inversify';
import { REPOSITORY_IDENTIFIER, SERVICE_IDENTIFIER } from 'src/constants';
import { ILogger } from 'src/logger';
import { EntityCreationError } from 'src/error/entity';
import { ClientUser, ServiceUser, Token } from 'src/entity';
import { exists } from 'src/util';
import { Transaction } from 'typeorm';
import { ITokenService } from 'src/service/tokenService';
import { IClientUserRepository, IServiceUserRepository, IRoleRepository, ITokenRepository } from 'src/repository';
import { InvalidInviteError, UserNotFoundError, InvalidUserPassword, UsernameTakenError, InvalidAccountTypeError } from 'src/error/controller';
import { IDiscordApiService } from 'src/service/discordApi';
import { addDays } from 'date-fns';

export interface CreateClientUserProps {
  discordId: string;
  discordToken: string;
  discordRefreshToken: string;
  roleNames: Array<string>;
}

export interface CreateServiceUserProps {
  serviceName: string;
  roleNames: Array<string>;
}

export interface ClientUserTokenData {
  discordId: string;
  discordToken: string;
  discordRefreshToken: string;
}

export interface IUserController {
  loginClientUser(code: string): Promise<string>;
}

@injectable()
export class UserController implements IUserController {
  private readonly logger: ILogger;
  private readonly tokenService: ITokenService;
  private readonly roleRepository: IRoleRepository;
  private readonly tokenRepository: ITokenRepository;
  private readonly discordApiService: IDiscordApiService;
  private readonly clientUserRepository: IClientUserRepository;
  private readonly serviceUserRepository: IServiceUserRepository;


  public constructor(
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
    @inject(SERVICE_IDENTIFIER.ITokenService) tokenService: ITokenService,
    @inject(REPOSITORY_IDENTIFIER.IRoleRepository) roleRepository: IRoleRepository,
    @inject(REPOSITORY_IDENTIFIER.ITokenRepository) tokenRepository: ITokenRepository,
    @inject(SERVICE_IDENTIFIER.IDiscordApiService) discordApiService: IDiscordApiService,
    @inject(REPOSITORY_IDENTIFIER.IClientUserRepository) clientUserRepository: IClientUserRepository,
    @inject(REPOSITORY_IDENTIFIER.IServiceUserRepository) serviceUserRepository: IServiceUserRepository,
  ) {
    this.logger = logger;
    this.tokenService = tokenService;
    this.roleRepository = roleRepository;
    this.tokenRepository = tokenRepository;
    this.discordApiService = discordApiService;
    this.clientUserRepository = clientUserRepository;
    this.serviceUserRepository = serviceUserRepository;
  }

  /**
   * Attempts to login a user to the app, using the data from discord API.
   * If user exists, updates his discord token data and creates a new JWT for site access,
   * if user does not exist, creates one with basic permissions, then assigns a new JWT.
   *
   * @returns user jwt
   * @param code discord oauth code
   */
  @Transaction()
  public async loginClientUser(code: string) {
    const discordTokenData = await this.discordApiService.exchangeCode(code);
    const discordUserData = await this.discordApiService.getDiscordUser(discordTokenData.accessToken);

    let user = await this.clientUserRepository.getByDiscordId(discordUserData.id);
    if (exists(user)) {
      user.discordToken = discordTokenData.accessToken;
      user.discordRefreshToken = discordTokenData.refreshToken;
    } else {
      const role = await this.roleRepository.getByName('Default');
      user = ClientUser.create({
        discordId: discordUserData.id,
        discordToken: discordTokenData.accessToken,
        discordRefreshToken: discordTokenData.refreshToken,
        roles: [role],
      });
    }

    const jwt = this.tokenService.generateToken(null, addDays(new Date(), 30));
    const userToken = Token.create({
      value: jwt,
      active: true,
      user,
    });

    await this.clientUserRepository.save(user);
    await this.tokenRepository.save(userToken);

    return jwt;
  }
}
