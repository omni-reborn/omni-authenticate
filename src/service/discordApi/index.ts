import { injectable, inject } from 'inversify';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { IDiscordApiServiceConfig } from 'src/config/service';
import { DiscordTokenData, DiscordAccessTokenResponse, DiscordUserResponse } from './interfaces';
import axios from 'axios';
import * as url from 'url';

export interface IDiscordApiService {
  exchangeCode(code: string): Promise<DiscordTokenData>;
  refreshAccessToken(refreshToken: string): Promise<DiscordTokenData>;
  getDiscordUser(token: string): Promise<DiscordUserResponse>;
}

@injectable()
export class DiscordApiService implements IDiscordApiService {
  private readonly config: IDiscordApiServiceConfig;

  public constructor(@inject(CONFIG_IDENTIFIER.IDiscordApiServiceConfig) config: IDiscordApiServiceConfig) {
    this.config = config;
  }

  public async exchangeCode(code: string) {
    const response = await axios.post<DiscordAccessTokenResponse>(url.resolve(this.config.discordApiEndpoint, '/oauth2/token'), {
      client_id: this.config.client.id,
      client_secret: this.config.client.secret,
      redirect_uri: this.config.appRedirectUri,
      grant_type: 'authorization_code',
      scope: 'identify',
      code,
    }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });

    const responseData = JSON.stringify(response.data);
    const responseObject = JSON.parse(responseData);

    return {
      accessToken: responseObject.data.access_token,
      refreshToken: responseObject.data.refresh_token,
    }
  }

  public async refreshAccessToken(refreshToken: string) {
    const response = await axios.post<DiscordAccessTokenResponse>(url.resolve(this.config.discordApiEndpoint, '/oauth2/token'), {
      client_id: this.config.client.id,
      client_secret: this.config.client.secret,
      redirect_uri: this.config.appRedirectUri,
      refresh_token: refreshToken,
      grant_type: 'refresh_token',
      scope: 'identify',
    }, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    });

    console.log('wow2');

    return {
      accessToken: response.data.access_token,
      refreshToken: response.data.refresh_token,
    }
  }

  public async getDiscordUser(token: string) {
    const response = await axios.get<DiscordUserResponse>(url.resolve(this.config.discordApiEndpoint, '/users/@me'), {
      headers: {
        'Authorization': `Bearer ${token}`,
      },
    });

    return response.data;
  }
}
