import * as jwt from 'jsonwebtoken';
import { injectable, inject } from 'inversify';
import { SERVICE_IDENTIFIER, CONFIG_IDENTIFIER } from 'src/constants';
import { ILogger } from 'src/logger';
import { ITokenServiceConfig } from 'src/config/service';
import { differenceInMilliseconds } from 'date-fns';
import { exists } from 'src/util';
import { InvalidTokenError } from 'src/error/service';

export interface UserToken {
  username: string;
}

export interface ITokenService {
  generateToken(data: object, expirationTime?: Date): string;
  verifyToken(token: string): boolean;
  decodeToken(token: string): UserToken;
}

@injectable()
export class TokenService implements ITokenService {
  private readonly config: ITokenServiceConfig;
  private readonly logger: ILogger;

  public constructor(
    @inject(CONFIG_IDENTIFIER.ITokenServiceConfig) config: ITokenServiceConfig,
    @inject(SERVICE_IDENTIFIER.ILogger) logger: ILogger,
  ) {
    this.config = config;
    this.logger = logger;
  }

  public generateToken(data: object, expirationTime?: Date) {
    // Default expiration time is 30 days
    const miliseconds = exists(expirationTime) ? differenceInMilliseconds(expirationTime, new Date()) : 1000 * 60 * 60 * 24 * 30;
    return jwt.sign(data, this.config['private-key'], { expiresIn: miliseconds });
  }

  public verifyToken(token: string) {
    try {
      jwt.verify(token, this.config['private-key']);
      return true;
    } catch (error) {
      return false;
    }
  }

  public decodeToken(token: string) {
    try {
      return jwt.verify(token, this.config['private-key']) as UserToken;
    } catch (error) {
      throw new InvalidTokenError();
    }
  }
}
