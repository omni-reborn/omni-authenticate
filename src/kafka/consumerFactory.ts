import { Kafka, ConsumerConfig, Consumer } from 'kafkajs';
import { inject, injectable } from 'inversify';
import { SERVICE_IDENTIFIER } from 'src/constants';
import { IKafkaConnector } from './kafkaConnector';

export interface IConsumerFactory {
  createConsumer(config?: ConsumerConfig): Consumer;
}

@injectable()
export class ConsumerFactory implements IConsumerFactory {
  private readonly client: Kafka;

  public constructor(@inject(SERVICE_IDENTIFIER.IKafkaConnector) kafkaConnector: IKafkaConnector) {
    this.client = kafkaConnector.getClient();
  }

  public createConsumer(config?: ConsumerConfig) {
    return this.client.consumer(config);
  }
}
