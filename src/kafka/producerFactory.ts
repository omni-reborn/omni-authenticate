import { Kafka, ProducerConfig, Producer } from 'kafkajs';
import { inject, injectable } from 'inversify';
import { SERVICE_IDENTIFIER } from 'src/constants';
import { IKafkaConnector } from './kafkaConnector';

export interface IProducerFactory {
  createProducer(config?: ProducerConfig): Producer;
}

@injectable()
export class ProducerFactory implements IProducerFactory {
  private readonly client: Kafka;

  public constructor(@inject(SERVICE_IDENTIFIER.IKafkaConnector) kafkaConnector: IKafkaConnector) {
    this.client = kafkaConnector.getClient();
  }

  public createProducer(config?: ProducerConfig) {
    return this.client.producer(config);
  }
}
