import { BaseRepository } from './core';
import { injectable } from 'inversify';
import { Token } from 'src/entity';
import { DatabaseError } from 'src/error/database';

export interface ITokenRepository {
  save(token: Token): Promise<void>;
  getByValue(value: string): Promise<Token>;
  getActiveByUsernameBulk(username: string): Promise<Array<Token>>;
}

@injectable()
export class TokenRepository extends BaseRepository implements ITokenRepository {
  private get repository() {
    return this.entityManager.getRepository(Token);
  }

  public async save(token: Token) {
    try {
      await this.repository.save(token);
    } catch (error) {
      throw new DatabaseError('Failed to save token to database.', error);
    }
  }

  public async getByValue(value: string) {
    try {
      return await this.repository.findOne({ where: { value } });
    } catch (error) {
      throw new DatabaseError('Failed to fetch token from the database.', error);
    }
  }

  public async getActiveByUsernameBulk(username: string) {
    try {
      return await this.repository.find({
        where: {
          active: true,
          user: { username },
        },
      });
    } catch (error) {
      throw new DatabaseError('Failed to fetch tokens from the database.', error);
    }
  }
}
