import { BaseRepository } from './core';
import { injectable } from 'inversify';
import { ClientUser } from 'src/entity';
import { DatabaseError } from 'src/error/database';

export interface IClientUserRepository {
  save(user: ClientUser): Promise<void>;
  getByDiscordId(discordId: string): Promise<ClientUser>;
}

@injectable()
export class ClientUserRepository extends BaseRepository implements IClientUserRepository {
  private get repository() {
    return this.entityManager.getRepository(ClientUser);
  }

  public async save(user: ClientUser) {
    try {
      await this.repository.save(user);
    } catch (error) {
      throw new DatabaseError('Failed to save client user to database.', error);
    }
  }

  public async getByDiscordId(discordId: string) {
    try {
      return await this.repository.findOne({ where: { discordId } });
    } catch (error) {
      throw new DatabaseError('Failed to fetch client user from database.', error);
    }
  }
}
