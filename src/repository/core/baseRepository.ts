import { EntityManager, getManager } from 'typeorm';
import { getNamespace } from 'cls-hooked';
import { exists } from 'src/util';
import { APP_IDENTIFIER } from 'src/constants';
import { injectable } from 'inversify';

@injectable()
export abstract class BaseRepository {
  protected get entityManager(): EntityManager {
    const context = getNamespace(APP_IDENTIFIER.clsContext);

    if (exists(context) && context.active) {
      const transactionalEntityManager = context.get(APP_IDENTIFIER.transactionalEntityManager);

      if (exists(transactionalEntityManager)) {
        return transactionalEntityManager;
      }
    }

    return getManager();
  }
}
