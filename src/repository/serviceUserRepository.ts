import { BaseRepository } from './core';
import { injectable } from 'inversify';
import { ServiceUser } from 'src/entity';
import { DatabaseError } from 'src/error/database';

export interface IServiceUserRepository {
  save(user: ServiceUser): Promise<void>;
  getByServiceName(serviceName: string): Promise<ServiceUser>;
}

@injectable()
export class ServiceUserRepository extends BaseRepository implements IServiceUserRepository {
  private get repository() {
    return this.entityManager.getRepository(ServiceUser);
  }

  public async save(user: ServiceUser) {
    try {
      await this.repository.save(user);
    } catch (error) {
      throw new DatabaseError('Failed to save service user to database.', error);
    }
  }

  public async getByServiceName(serviceName: string) {
    try {
      return await this.repository.findOne({ where: { serviceName } });
    } catch (error) {
      throw new DatabaseError('Failed to fetch service user from the database.', error);
    }
  }
}
