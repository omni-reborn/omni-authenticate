import { BaseRepository } from './core';
import { injectable } from 'inversify';
import { Role } from 'src/entity';
import { DatabaseError } from 'src/error/database';
import { In } from 'typeorm';

export interface IRoleRepository {
  save(role: Role): Promise<void>;
  getByName(name: string): Promise<Role>;
  getByNameBulk(names: Array<string>): Promise<Array<Role>>;
}

@injectable()
export class RoleRepository extends BaseRepository implements IRoleRepository {
  private get repository() {
    return this.entityManager.getRepository(Role);
  }

  public async save(role: Role) {
    try {
      await this.repository.save(role);
    } catch (error) {
      throw new DatabaseError('Failed to save role to database.', error);
    }
  }

  public async getByName(name: string) {
    try {
      return await this.repository.findOne({ where: { name } });
    } catch (error) {
      throw new DatabaseError('Failed to fetch role from database.', error);
    }
  }

  public async getByNameBulk(names: Array<string>) {
    try {
      return await this.repository.find({
        where: { name: In(names) },
      });
    } catch (error) {
      throw new DatabaseError('Failed to fetch roles from database.', error);
    }
  }
}
