import { BaseRepository } from './core';
import { injectable } from 'inversify';
import { Permission } from 'src/entity';
import { In } from 'typeorm';
import { DatabaseError } from 'src/error/database';

export interface IPermissionRepository {
  save(permission: Permission): Promise<void>;
  getByPermissionStringBulk(permissionStrings: Array<string>): Promise<Array<Permission>>;
}

@injectable()
export class PermissionRepository extends BaseRepository implements IPermissionRepository {
  private get repository() {
    return this.entityManager.getRepository(Permission);
  }

  public async save(permission: Permission) {
    try {
      await this.repository.save(permission);
    } catch (error) {
      throw new DatabaseError('Failed to save permission to database.', error);
    }
  }

  public getByPermissionStringBulk(permissionStrings: Array<string>) {
    try {
      return this.repository.find({
        where: { permissionString: In(permissionStrings) },
      });
    } catch (error) {
      throw new DatabaseError('Failed to fetch permissions from database.', error);
    }
  }
}
