import { getConnection } from 'typeorm';
import { getNamespace } from 'cls-hooked';
import { exists } from './exists';
import { TransactionError } from 'src/error/database';
import { APP_IDENTIFIER } from 'src/constants';

export function Transaction(): MethodDecorator {
  return (target: Object, methodName: string, descriptor?: PropertyDescriptor) => {
    const originalMethod = descriptor.value;

    descriptor.value = (...args: Array<any>) => {
      return getConnection().transaction(async (entityManager) => {
        const context = getNamespace(APP_IDENTIFIER.clsContext);

        if (!exists(context)) {
          // This will happen if no CLS namespace has been initialied in your app.
          // At application startup, you need to create a CLS namespace using createNamespace(...) function.
          throw new TransactionError('No CLS namespace defined in your app ... Cannot use CLS transaction management.');
        }

        if (!context.active) {
          // This will happen if your code has not been executed using the run(...) function of your CLS
          // namespace.
          // Example: the code triggered in your app by an entry HTTP request (or whatever other entry event,
          // like one triggered by a message dropped in a queue your app is listening at), should be wrapped
          // using the run(...) function of your CLS namespace.
          // Using run(...) ensures that an active context is set, where you can safely store and retrieve things.
          throw new TransactionError('No CLS active context detected ... Cannot use CLS transaction management.')
        }

        /* tslint:disable: no-invalid-this */
        context.set(APP_IDENTIFIER.transactionalEntityManager, entityManager);
        const result = await originalMethod.apply(this, [...args]);

        context.set(APP_IDENTIFIER.transactionalEntityManager, null);
        return result;
      });
    }
  }
}
