export function exists(value: any) {
  if (value !== null && value !== undefined) {
    return true;
  }

  return false;
}

export function existsBulk(values: Array<any>) {
  for (const value of values) {
    if (value === null || value === undefined) {
      return false;
    }
  }

  return true;
}
