import * as uuid from 'uuid/v4';
import { Entity, PrimaryColumn, Column } from 'typeorm';
import { EntityCreationError } from 'src/error/entity';
import { exists } from 'src/util';

export interface PermissionCreationProps {
  id?: string;
  permissionString: string;
}

@Entity({ name: 'Permission' })
export class Permission {
  @PrimaryColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  public permissionString: string;

  private static isValidPermissionString(value: string) {
    const regex = /^(\w+|\*)(:(\w+|\*))*$/g;
    return regex.test(value);
  }

  public static create(props: PermissionCreationProps) {
    if (!this.isValidPermissionString(props.permissionString)) {
      throw new EntityCreationError('Failed to create permission. Invalid permission string.');
    }

    const permission = new Permission();
    permission.id = exists(props.id) ? props.id : uuid();
    permission.permissionString = props.permissionString;

    return permission;
  }
}
