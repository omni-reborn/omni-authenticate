import * as uuid from 'uuid/v4';
import { Role } from './role';
import { exists } from 'src/util';
import { Token } from './token';
import { Column, PrimaryGeneratedColumn, Entity, ManyToMany, JoinTable, OneToMany } from 'typeorm';

export interface CreateUserProps {
  id?: string;
  discordId: string;
  discordToken?: string;
  discordRefreshToken?: string;
  roles: Array<Role>;
}

@Entity({ name: 'ClientUser' })
export class ClientUser {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column()
  public discordId: string;

  @Column()
  public discordToken: string;

  @Column()
  public discordRefreshToken: string;

  @OneToMany(() => Token, (token: Token) => token.clientUser)
  public tokens: Array<Token>;

  @ManyToMany(() => Role)
  @JoinTable()
  public roles: Array<Role>;

  public static create(props: CreateUserProps) {
    const user = new ClientUser();
    user.id = exists(props.id) ? props.id : uuid();
    user.discordId = props.discordId;
    user.discordToken = props.discordToken;
    user.discordRefreshToken = props.discordRefreshToken;
    user.roles = props.roles;

    return user;
  }
}
