import * as uuid from 'uuid/v4';
import { Column, PrimaryColumn, Entity, ManyToOne } from 'typeorm';
import { exists } from 'src/util';
import { ServiceUser } from './serviceUser';
import { ClientUser } from './clientUser';

export interface CreateTokenProps {
  id?: string;
  value: string;
  active: boolean;
  user: ServiceUser | ClientUser;
}

@Entity({ name: 'Token' })
export class Token {
  @PrimaryColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  public value: string;

  @Column({ nullable: false })
  public active: boolean;

  @ManyToOne(() => ClientUser, (user: ClientUser) => user.tokens)
  public clientUser: ClientUser;

  @ManyToOne(() => ServiceUser, (user: ServiceUser) => user.tokens)
  public serviceUser: ServiceUser;

  public static create(props: CreateTokenProps) {
    const token = new Token();

    token.id = exists(props.id) ? props.id : uuid();
    token.value = props.value;
    token.active = props.active;
    token.clientUser = props.user instanceof ClientUser ? props.user : null;
    token.serviceUser = props.user instanceof ServiceUser ? props.user : null;

    return token;
  }
}
