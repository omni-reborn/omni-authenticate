import * as uuid from 'uuid/v4';
import * as crypto from 'crypto';
import { Role } from './role';
import { exists } from 'src/util';
import { Token } from './token';
import { PrimaryGeneratedColumn, Entity, ManyToMany, JoinTable, OneToMany, Column } from 'typeorm';

export interface CreateServiceUserProps {
  id?: string;
  serviceToken?: string;
  serviceName: string;
  roles: Array<Role>;
}

@Entity({ name: 'ServiceUser' })
export class ServiceUser {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column()
  public serviceName: string;

  @Column()
  public serviceToken: string;

  @OneToMany(() => Token, (token: Token) => token.serviceUser)
  public tokens: Array<Token>;

  @ManyToMany(() => Role)
  @JoinTable()
  public roles: Array<Role>;

  private static generateServiceToken() {
    return new Promise<string>((resolve, reject) => {
      crypto.randomBytes(64, (err, buffer) => {
        if (exists(err)) {
          reject(err);
        }

        resolve(buffer.toString('hex'));
      });
    });
  }

  public static async create(props: CreateServiceUserProps) {
    const user = new ServiceUser();
    user.id = exists(props.id) ? props.id : uuid();
    user.serviceName = props.serviceName;
    user.serviceToken = exists(props.serviceToken) ? props.serviceToken : await this.generateServiceToken();
    user.roles = props.roles;

    return user;
  }
}
