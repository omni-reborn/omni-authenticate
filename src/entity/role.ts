import * as uuid from 'uuid/v4';
import { Column, PrimaryColumn, Entity, ManyToMany, JoinTable } from 'typeorm';
import { Permission } from './permission';
import { EntityCreationError } from 'src/error/entity';
import { exists } from 'src/util';

export interface CreateRoleProps {
  id?: string;
  name: string;
  level: number;
  permissions?: Array<Permission>;
}

@Entity({ name: 'Role' })
export class Role {
  @PrimaryColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  public name: string;

  @Column({ nullable: false })
  public level: number;

  @ManyToMany(() => Permission)
  @JoinTable()
  public permissions: Array<Permission>;

  public static isValidLevel(value: number) {
    return !isNaN(value) && value >= 0;
  }

  public static create(props: CreateRoleProps) {
    if (!this.isValidLevel(props.level)) {
      throw new EntityCreationError('Cannot create role entity. Role level value is invalid.');
    }

    const role = new Role();
    role.id = exists(props.id) ? props.id : uuid();
    role.level = props.level;
    role.name = props.name;
    role.permissions = props.permissions;

    return role;
  }
}
