import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TTokenServiceConfig>;
export interface ITokenServiceConfig extends T { }

export const TTokenServiceConfig = t.type({
  'private-key': t.string,
});
