import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TDiscordApiServiceConfig>;
export interface IDiscordApiServiceConfig extends T { }

export const TClientConfig = t.type({
  id: t.string,
  secret: t.string,
});

export const TDiscordApiServiceConfig = t.type({
  discordApiEndpoint: t.string,
  appRedirectUri: t.string,
  client: TClientConfig,
});
