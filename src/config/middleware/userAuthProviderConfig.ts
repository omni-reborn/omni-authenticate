import * as t from 'io-ts';

/* tslint:disable: no-empty-interface */
type T = t.TypeOf<typeof TUserAuthProviderConfig>;
export interface IUserAuthProviderConfig extends T { }

export const TUserAuthProviderConfig = t.type({
  'user-cookie-name': t.string,
});
