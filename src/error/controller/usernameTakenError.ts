import { ControllerError } from './controllerError';

export class UsernameTakenError extends ControllerError {
  public constructor(message = 'Provided username is taken.', error?: any) {
    super(message, error);
  }
}
