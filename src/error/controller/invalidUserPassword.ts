import { ControllerError } from './controllerError';

export class InvalidUserPassword extends ControllerError {
  public constructor(message = 'Given user password is invalid.', error?: any) {
    super(message, error);
  }
}
