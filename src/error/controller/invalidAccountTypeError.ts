import { ControllerError } from './controllerError';

export class InvalidAccountTypeError extends ControllerError {
  public constructor(message = 'Provided account type is invalid for the operation requested.', error?: any) {
    super(message, error);
  }
}
