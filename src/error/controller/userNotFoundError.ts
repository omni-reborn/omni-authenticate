import { ControllerError } from './controllerError';

export class UserNotFoundError extends ControllerError {
  public constructor(message = 'User with given username does not exist.', error?: any) {
    super(message, error);
  }
}
