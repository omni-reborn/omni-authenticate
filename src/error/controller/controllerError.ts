import { BaseError } from 'src/error/core';

export class ControllerError extends BaseError {
  public constructor(message = 'Generic controller error.', error?: any) {
    super(message, error);
  }
}
