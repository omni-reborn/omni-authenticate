import { ControllerError } from './controllerError';

export class InvalidInviteError extends ControllerError {
  public constructor(message = 'Provided invite is invalid or expired.', error?: any) {
    super(message, error);
  }
}
