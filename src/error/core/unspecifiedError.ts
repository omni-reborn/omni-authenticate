import { BaseError } from './baseError';

export class UnspecifiedError extends BaseError {
  public constructor(message = 'Something went wrong.', error?: any) {
    super(message, error);
  }
}
