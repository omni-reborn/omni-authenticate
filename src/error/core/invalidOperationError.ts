import { BaseError } from './baseError';

export class InvalidOperationError extends BaseError {
  public constructor(message = 'Invalid operation.', error?: any) {
    super(message, error);
  }
}
