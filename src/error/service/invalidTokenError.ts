import { ServiceError } from './serviceError';

export class InvalidTokenError extends ServiceError {
  public constructor(message = 'Provided JWT token is invalid.', error?: any) {
    super(message, error);
  }
}
