import { BaseError } from 'src/error/core';

export class ServiceError extends BaseError {
  public constructor(message = 'Generic service error.', error?: any) {
    super(message, error);
  }
}
