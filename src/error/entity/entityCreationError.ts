import { EntityError } from './entityError';

export class EntityCreationError extends EntityError {
  public constructor(message = 'Generic entity creation error.', error?: any) {
    super(message, error);
  }
}
