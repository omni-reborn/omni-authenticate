import { BaseError } from 'src/error/core';

export class EntityError extends BaseError {
  public constructor(message = 'Generic entity error.', error?: any) {
    super(message, error);
  }
}
