import { SERVICE_IDENTIFIER, FACTORY_IDENTIFIER } from 'src/constants';
import { interfaces, ContainerModule } from 'inversify';
import { IKafkaConnector, KafkaConnector, IProducerFactory, IConsumerFactory, ConsumerFactory, ProducerFactory } from 'src/kafka';

export const kafkaModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IKafkaConnector>(SERVICE_IDENTIFIER.IKafkaConnector)
      .to(KafkaConnector)
      .inSingletonScope();

    bind<IConsumerFactory>(FACTORY_IDENTIFIER.IConsumerFactory)
      .to(ConsumerFactory)
      .inRequestScope();

    bind<IProducerFactory>(FACTORY_IDENTIFIER.IProducerFactory)
      .to(ProducerFactory)
      .inRequestScope();
  },
);
