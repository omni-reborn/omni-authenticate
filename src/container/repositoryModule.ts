import { REPOSITORY_IDENTIFIER } from 'src/constants';
import { interfaces, ContainerModule } from 'inversify';
import {
  IClientUserRepository,
  ClientUserRepository,
  IRoleRepository,
  RoleRepository,
  IPermissionRepository,
  PermissionRepository,
  ITokenRepository,
  TokenRepository,
  IServiceUserRepository,
  ServiceUserRepository,
} from 'src/repository';

export const repositoryModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IClientUserRepository>(REPOSITORY_IDENTIFIER.IClientUserRepository)
      .to(ClientUserRepository)
      .inSingletonScope();

    bind<IServiceUserRepository>(REPOSITORY_IDENTIFIER.IServiceUserRepository)
      .to(ServiceUserRepository)
      .inSingletonScope();

    bind<IRoleRepository>(REPOSITORY_IDENTIFIER.IRoleRepository)
      .to(RoleRepository)
      .inRequestScope();

    bind<ITokenRepository>(REPOSITORY_IDENTIFIER.ITokenRepository)
      .to(TokenRepository)
      .inRequestScope();

    bind<IPermissionRepository>(REPOSITORY_IDENTIFIER.IPermissionRepository)
      .to(PermissionRepository)
      .inRequestScope();
  },
);
