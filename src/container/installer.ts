import 'reflect-metadata';
import { Container } from 'inversify';
import { configModule } from './configModule';
import { databaseModule } from './databaseModule';
import { kafkaModule } from './kafkaModule';
import { serviceModule } from './serviceModule';
import { repositoryModule } from './repositoryModule';
import { controllerModule } from './controllerModule';

export async function installContainer(): Promise<Container> {
  const container = new Container();
  container.load(configModule);
  container.load(databaseModule);
  container.load(kafkaModule);
  container.load(repositoryModule);
  container.load(controllerModule);
  container.load(serviceModule);

  return container;
}
