import { SERVICE_IDENTIFIER, CONFIG_IDENTIFIER, FACTORY_IDENTIFIER } from 'src/constants';
import { interfaces, ContainerModule } from 'inversify';
import { ILogger, Logger, ILoggerFactory, LoggerFactory } from 'src/logger';
import { IAuthService, AuthService } from 'src/service';
import { ILoggerConfig } from 'src/config/logger';
import { ITokenService, TokenService } from 'src/service/tokenService';
import { DiscordApiService, IDiscordApiService } from 'src/service/discordApi';

export const serviceModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<ILogger>(SERVICE_IDENTIFIER.ILogger)
      .toDynamicValue((context: interfaces.Context) => {
        const config = context.container.get<ILoggerConfig>((CONFIG_IDENTIFIER.ILoggerConfig));
        return new Logger(config, 'application');
      })
      .inSingletonScope();

    bind<ILoggerFactory>(FACTORY_IDENTIFIER.ILoggerFactory)
      .to(LoggerFactory)
      .inRequestScope();

    bind<IAuthService>(SERVICE_IDENTIFIER.IAuthService)
      .to(AuthService)
      .inRequestScope();

    bind<ITokenService>(SERVICE_IDENTIFIER.ITokenService)
      .to(TokenService)
      .inRequestScope();

    bind<IDiscordApiService>(SERVICE_IDENTIFIER.IDiscordApiService)
      .to(DiscordApiService)
      .inRequestScope();
  },
);
