import { ContainerModule, interfaces } from 'inversify';
import { IDatabaseConnectorConfig, TDatabaseConnectorConfig } from 'src/config/database';
import { CONFIG_IDENTIFIER } from 'src/constants';
import { loadConfig } from 'src/config/core';
import { IKafkaConnectorConfig, TKafkaConnectorConfig } from 'src/config/kafka';
import { IAppServiceConfig, TAppServiceConfig } from 'src/config/app';
import { ILoggerConfig, TLoggerConfig } from 'src/config/logger';
import { IAuthServiceConfig, TAuthServiceConfig, ITokenServiceConfig, TTokenServiceConfig, IDiscordApiServiceConfig, TDiscordApiServiceConfig } from 'src/config/service';
import { IUserAuthProviderConfig, TUserAuthProviderConfig } from 'src/config/middleware/userAuthProviderConfig';

export const configModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IDatabaseConnectorConfig>(CONFIG_IDENTIFIER.IDatabaseConnectorConfig)
      .toConstantValue(loadConfig<IDatabaseConnectorConfig>(TDatabaseConnectorConfig, '/usr/src/app/config/databaseConfig.yml'));

    bind<IKafkaConnectorConfig>(CONFIG_IDENTIFIER.IKafkaConnectorConfig)
      .toConstantValue(loadConfig<IKafkaConnectorConfig>(TKafkaConnectorConfig, '/usr/src/app/config/kafkaConfig.yml'));

    bind<IAppServiceConfig>(CONFIG_IDENTIFIER.IAppServiceConfig)
      .toConstantValue(loadConfig<IAppServiceConfig>(TAppServiceConfig, '/usr/src/app/config/appServiceConfig.yml'));

    bind<IAuthServiceConfig>(CONFIG_IDENTIFIER.IAuthServiceConfig)
      .toConstantValue(loadConfig<IAuthServiceConfig>(TAuthServiceConfig, '/usr/src/app/config/authServiceConfig.yml'));

    bind<ILoggerConfig>(CONFIG_IDENTIFIER.ILoggerConfig)
      .toConstantValue(loadConfig<ILoggerConfig>(TLoggerConfig, '/usr/src/app/config/loggerConfig.yml'));

    bind<IUserAuthProviderConfig>(CONFIG_IDENTIFIER.IUserAuthProviderConfig)
      .toConstantValue(loadConfig<IUserAuthProviderConfig>(TUserAuthProviderConfig, '/usr/src/app/config/userAuthProviderConfig.yml'));

    bind<ITokenServiceConfig>(CONFIG_IDENTIFIER.ITokenServiceConfig)
      .toConstantValue(loadConfig<ITokenServiceConfig>(TTokenServiceConfig, '/usr/src/app/config/tokenServiceConfig.yml'));

    bind<IDiscordApiServiceConfig>(CONFIG_IDENTIFIER.IDiscordApiServiceConfig)
      .toConstantValue(loadConfig<IDiscordApiServiceConfig>(TDiscordApiServiceConfig, '/usr/src/app/config/discordApiServiceConfig.yml'));
  },
);
