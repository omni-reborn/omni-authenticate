import { CONTROLLER_IDENTIFIER } from 'src/constants';
import { interfaces, ContainerModule } from 'inversify';
import { IUserController, UserController, IRoleController, RoleController } from 'src/controller/domain';

export const controllerModule = new ContainerModule(
  (bind: interfaces.Bind) => {
    bind<IUserController>(CONTROLLER_IDENTIFIER.IUserController)
      .to(UserController)
      .inRequestScope();

    bind<IRoleController>(CONTROLLER_IDENTIFIER.IRoleController)
      .to(RoleController)
      .inRequestScope();
  },
);
