export const REPOSITORY_IDENTIFIER = {
  IRoleRepository: Symbol.for('IRoleRepository'),
  ITokenRepository: Symbol.for('ITokenRepository'),
  IPermissionRepository: Symbol.for('IPermissionRepository'),
  IClientUserRepository: Symbol.for('IClientUserRepository'),
  IServiceUserRepository: Symbol.for('IServiceUserRepository'),
}
