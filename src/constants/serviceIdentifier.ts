export const SERVICE_IDENTIFIER = {
  IDatabaseConnector: Symbol.for('IDatabaseConnector'),
  IKafkaConnector: Symbol.for('IKafkaConnector'),
  IAppService: Symbol.for('IAppService'),
  IAuthService: Symbol.for('IAuthService'),
  ITokenService: Symbol.for('ITokenService'),
  IDiscordApiService: Symbol.for('IDiscordApiService'),
  ILogger: Symbol.for('ILogger'),
};
