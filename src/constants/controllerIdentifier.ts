export const CONTROLLER_IDENTIFIER = {
  IUserController: Symbol.for('IUserController'),
  IRoleController: Symbol.for('IRoleController'),
};
