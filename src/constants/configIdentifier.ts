export const CONFIG_IDENTIFIER = {
  IDatabaseConnectorConfig: Symbol.for('IDatabaseConnectorConfig'),
  IKafkaConnectorConfig: Symbol.for('IKafkaConnectorConfig'),
  IAppServiceConfig: Symbol.for('IAppServiceConfig'),
  IAuthServiceConfig: Symbol.for('IAuthServiceConfig'),
  ITokenServiceConfig: Symbol.for('ITokenServiceConfig'),
  IDiscordApiServiceConfig: Symbol.for('IDiscordApiServiceConfig'),
  IUserAuthProviderConfig: Symbol.for('IUserAuthProviderConfig'),
  ILoggerConfig: Symbol.for('ILoggerConfig'),
};
