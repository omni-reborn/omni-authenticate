import { interfaces } from 'inversify-express-utils';
import { UserDetails } from 'src/service';
import * as shiro from 'shiro-trie';
import { flatten } from 'lodash';

export class Principal implements interfaces.Principal {
  private readonly account: shiro.ShiroTrie;
  public readonly details: UserDetails;

  public constructor(details: UserDetails) {
    this.account = shiro.newTrie();
    this.details = details;

    const permissions = flatten(this.details.roles.map((role) => role.permissions));
    this.account.add(...permissions);
  }

  public async isAuthenticated() {
    return this.details.isAuthenticated;
  }

  public async isResourceOwner(resource: string) {
    return this.account.check(resource);
  }

  public async isInRole(role: string) {
    const roles = this.details.roles.map((r) => r.name);
    return roles.includes(role);
  }
}
